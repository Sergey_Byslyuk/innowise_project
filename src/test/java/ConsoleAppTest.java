import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import static org.junit.Assert.*;

public class ConsoleAppTest {

    private ConsoleApp consoleApp;

    @Before
    public void initTest() {
        consoleApp = new ConsoleApp();
    }

    @After
    public void afterTest() {
        consoleApp = null;
    }


    @Test
    public void testInputEmail() {
        String email = "stan@gmail.com";
        assertEquals(email, consoleApp.inputEmail(email));
        email = "stan@gmailcom";
        assertNotEquals(email, consoleApp.inputEmail(email));
        email = "stangmail.com";
        assertNotEquals(email, consoleApp.inputEmail(email));
    }

    @Test
    public void testInputRoles() {
        String roles = "admin developer";
        assertEquals(2, consoleApp.inputRoles(roles).size());
        assertEquals("admin", consoleApp.inputRoles(roles).get(0));
        assertEquals("developer", consoleApp.inputRoles(roles).get(1));

        roles = "admin developer manager";
        assertEquals(0, consoleApp.inputRoles(roles).size());

        roles = "admin";
        assertEquals(1, consoleApp.inputRoles(roles).size());
        assertEquals("admin", consoleApp.inputRoles(roles).get(0));

        roles = "admin  developer";
        assertEquals(0, consoleApp.inputRoles(roles).size());

    }

    @Ignore
    @Test
    public void testInputPhones() {
        String phones = "37544 4932944";
        assertEquals(1, consoleApp.inputPhones(phones).size());
        assertEquals("37544 4932944", consoleApp.inputPhones(phones).get(0));

        phones = "37544 4932944 37544 4932944";
        assertEquals(2, consoleApp.inputPhones(phones).size());
        assertEquals("37544 4932944", consoleApp.inputPhones(phones).get(0));
        assertEquals("37544 4932944", consoleApp.inputPhones(phones).get(1));

        phones = "37544 4932944 37544 4932944 37544 4132944";
        assertEquals(0, consoleApp.inputPhones(phones).size());

        phones = "37544 123";
        assertEquals(0, consoleApp.inputPhones(phones).size());

        phones = "37625 3431221";
        assertEquals(0, consoleApp.inputPhones(phones).size());
    }
}