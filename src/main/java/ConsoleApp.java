import entity.User;

import java.io.*;
import java.util.*;

public class ConsoleApp {

    public static final String CREATE_USER = "1";
    public static final String EDIT_USER = "2";
    public static final String SHOW_USER = "3";
    public static final String DELETE_USER = "4";

    public List<User> users = new ArrayList<>();

    public Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        ConsoleApp app = new ConsoleApp();
        app.runTheConsole();
    }

    public void runTheConsole() {
        System.out.println("Choose option:");
        System.out.println("1.CREATE USER");
        System.out.println("2.EDIT USER");
        System.out.println("3.SHOW USERS");
        System.out.println("4.DELETE USER");
        String action = scanner.nextLine();
        readFile();

        switch (action) {
            case CREATE_USER:
                User user = createUser();
                System.out.println("Save(y/n)? ");
                String answer = scanner.nextLine();
                if (answer.equals("y")) {
                    users.add(user);
                    editFile();
                    System.out.println("New user saved.");
                }
                break;
            case EDIT_USER:
                editUser();
                break;
            case SHOW_USER:
                showUsers();
                break;
            case DELETE_USER:
                deleteUser();

                break;
            default:
                System.out.println("Invalid input!");
                runTheConsole();
        }
        scanner.close();
    }

    public void editUser() {
        showUsers();
        System.out.println("Choose number of user for edit.");
        Scanner scan = new Scanner(System.in);
        int number = 0;
        try {
            number = scan.nextInt();
        } catch (Exception e) {
            System.out.println("Invalid number.");
            editUser();
        }
        if (number > 0 && number <= users.size()) {
            User user = createUser();
            users.set(number - 1, user);
            editFile();
            System.out.println("User was edited.");
        } else {
            System.out.println("Invalid number of user.");
            editUser();
        }
    }

    public void deleteUser() {
        showUsers();
        System.out.println("Choose number of user for delete.");
        Scanner scan = new Scanner(System.in);
        int number = 0;
        try {
            number = scan.nextInt();
        } catch (Exception e) {
            System.out.println("Invalid number.");
            deleteUser();
        }
        if (number > 0 && number <= users.size()) {
            users.remove(number - 1);
            editFile();
            System.out.println("User was removed.");
        } else {
            System.out.println("Invalid number");
            runTheConsole();
        }
    }

    public User createUser() {
        System.out.println("Creating user");
        String message = "First name: ";
        String firstName = input(message);
        message = "Last name: ";
        String lastName = input(message);
        message = "Email: ";
        String email = null;
        while (email == null) {
            email = inputEmail(input(message));
        }
        message = "Enter space-separated roles(Example:'admin manager').\nRoles: ";
        List<String> roles = new ArrayList<>();
        while (roles.size() == 0) {
            roles = inputRoles(input(message));
        }
        message = "Enter space-separated phone numbers(Example:'37544 3212211 37525 7574433').\nPhones: ";
        List<String> phones = new ArrayList<>();
        while (phones.size() == 0) {
            phones = inputPhones(input(message));
        }
        User user = new User(firstName, lastName, email, roles, phones);
        System.out.println(user.toString());
        return user;
    }

    public String inputEmail(String email) {
        if (!(email.indexOf("@") != -1 && email.indexOf(".") != -1)) {
            System.out.println("Incorrect email. Email must contain '@' and '.'!");
            return null;
        }
        return email;
    }

    public List<String> inputPhones(String phones) {
        List<String> listPhones = new ArrayList<>();
        if (phones.length() == 13 || (phones.length() - 1) % 13 == 0) {
            for (int i = 0; i <= phones.length() / 13 - 1; i++) {
                char c = phones.charAt(5 + (14 * i));
                if (c == ' ') {
                    String phone = phones.substring(i * 14, 14 * (i + 1) - 1);
                    String first = phone.substring(0, 5);
                    String second = phone.substring(6, 13);
                    if (first.matches("[0-9]+") == second.matches("[0-9]+") == true && first.startsWith("375")) {
                        listPhones.add(phone);
                    } else {
                        System.out.println("Incorrect phone numbers.");
                        return listPhones;
                    }
                } else {
                    System.out.println("Incorrect phone numbers.");
                    return listPhones;
                }
            }
        } else if (phones.length() % 13 > 1 && phones.length()/ 13>2) {
            System.out.println("Incorrect number of mobile phones. Must be less than 3 phones.");
            return listPhones;
        } else {
            System.out.println("Incorrect phone numbers.");
            return listPhones;
        }
        return listPhones;
    }

    public List<String> inputRoles(String roles) {
        int space = 0;
        List<String> listRoles = new ArrayList<>();
        for (int i = 0; i < roles.length(); i++) {
            if (roles.charAt(i) == ' ') {
                if (roles.charAt(i-1) != ' '){
                    String role = roles.substring(space, i);
                    listRoles.add(role);
                    space = i + 1;
                }else {
                    System.out.println("Incorrect input.");
                    listRoles.clear();
                    return listRoles;
                }
            }else if (i==roles.length()-1){
                String role = roles.substring(space, i+1);
                listRoles.add(role);
            }
        }
        if (listRoles.size()>2) {
            System.out.println("Incorrect number of roles. Must be less than 3 roles.");
            listRoles.clear();
            return listRoles;
        }
        return listRoles;
    }

    public String input(String message) {
        System.out.print(message);
        String text = scanner.nextLine();
        if (text.length() == 0 && text.startsWith(" ")) {
            System.out.println("Incorrect text.");
            input(message);
        }
        return text.trim();
    }

    public void editFile() {
        try (ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream("users.txt"))) {
            oos.writeObject(users);
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }
    }

    public void readFile() {
        try (ObjectInputStream ois = new ObjectInputStream(new FileInputStream("users.txt"))) {

            users = ((ArrayList<User>) ois.readObject());

        } catch (Exception ex) {

            System.out.println(ex.getMessage());
        }
    }

    public void showUsers() {
        for (int i = 0; i < users.size(); i++) {
            System.out.println(i + 1 + ". " + users.get(i));
        }
    }
}
